Guide to create environment variables.
--------------------------------------

Step 1: Go to Heroku.com and overview of your application

Step 2: Go to Settings

Step 3: Go to Reveal Config Vars

Step 4: Under "Config Vars", put 'API_KEY' in "KEY" window and choose a random key for "VALUE" window and click "Add"

Step 5: Put 'NODE_ENV' in the new "KEY" window and 'production' in the corresponding "VALUE" window and click "Add"

Step 6: Go to you root project folder. Create a new file ".env" in root.

Step 7: In .env file, put,

REACT_APP_API_KEY= 'Your selected kay'
REACT_APP_API_URL= 'URL to your Heroku app'

Step 8: Go to file .gitignore and add '.env' under #misc