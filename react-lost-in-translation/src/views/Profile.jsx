import ClearListButton from "../components/Profile/ClearListButton";
import Header from "../components/Profile/Header";
import OutputHistory from "../components/Profile/OutputHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../HOC/withAuth";

const Profile = () => {
  const { user } = useUser();

  return (
    <div className="profile">
      <div className="profile-header">
        <Header username={user.username} />
      </div>

      <div>
        <OutputHistory translations={user.translations} />
      </div>
      <div class="clBtnContainer">
        <ClearListButton />
      </div>
    </div>
  );
};
export default withAuth(Profile);
