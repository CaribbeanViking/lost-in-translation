import { addTranslation } from "../api/translations";
import InputForm from "../components/Translator/InputForm";
import OutputTranslation from "../components/Translator/OutputTranslation"
import { useUser } from '../context/UserContext'
import { storageSave } from "../utils/storage";
import withAuth from "../HOC/withAuth";
import { STORAGE_KEY_USER } from "../const/storageKeys";

const Translation = () => {

    // User Access
    const { user, setUser } = useUser();

    const handleTranslation = async (data) => {

        const [error, result] = await addTranslation(user, data)
        
        if(result !== null){
            setUser(result);
            storageSave(STORAGE_KEY_USER, result);
        } 
  }

    return(
        <div className="translator">
            <InputForm onTranslate={ handleTranslation }/>
        </div>
    )
}
export default withAuth(Translation);