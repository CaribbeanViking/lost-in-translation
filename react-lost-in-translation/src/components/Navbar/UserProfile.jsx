import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";

export default function UserProfile() {
  const { user } = useUser();
  if (user !== null) {
    return (
      <div>
        <button class="navbar-button user-profile-button" id="toProfile">
          <NavLink to="/profile" className='navlink' >Profile</NavLink>
        </button>
      </div>
    );
  }
  return null;
}
