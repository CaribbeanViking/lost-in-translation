import UserProfile from "./UserProfile.jsx";
import ToTranslation from "./ToTranslation.jsx";

export default function NavBar() {
  return (
    <div class="navbar">
      <h1 class="logo-text">Lost in translation.</h1>
      <ToTranslation />
      <UserProfile />
    </div>
  );
}
