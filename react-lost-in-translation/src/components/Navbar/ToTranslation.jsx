import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";

export default function ToTranslation() {

  const { user } = useUser();
  if (user !== null) {
    return (
      <div>
        <button
          class="navbar-button lost-in-translation-button"
          id="toTranslation"
        >
          <NavLink to="/translation" className="navlink">Translator</NavLink>
        </button>
      </div>
    );
  }
  return null;
}
