//import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
//import { loginUser } from "../../api/user";
import { useUser } from "../../context/UserContext";

import { useState, useEffect } from "react";
import { loginUser } from "../../api/user";
import { storageSave } from "../../utils/storage";
//import { useHistory } from "react-router-dom";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

const usernameConfig = {
  required: true,
  minLength: 2,
};

export default function LoginForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const { user, setUser } = useUser();
  const navigate = useNavigate();

  useEffect(() => {
    if (user !== null) {
      navigate("translation");
      console.log("User has changed!", user);
    }
  }, [user, navigate]);

  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  const errorCheck = (() => {
    if (!errors.username) {
      return null;
    }

    if (errors.username.type === "required") {
      return <span className="error-msg">Username is required!</span>;
    }

    if (errors.username.type === "minLength") {
      return <span className="error-msg">Username is too short!</span>;
    }
  })();

  return (
    <div className="login-box">
      <form className="login-form" onSubmit={handleSubmit(onSubmit)}>
        <fieldset className="login-fieldset">
          <label htmlFor="username"></label>
          <div className="innerLoginBox">
            <input
              className="login-input"
              type="text"
              autocomplete="off"
              placeholder="What's your name?"
              {...register("username", usernameConfig)}
            />
            <button
              type="submit"
              disabled={loading}
              className="button login-button"
            >
              Let's go!
            </button>
          </div>
          <hr className="hr" />
          <div className="error-div">{errorCheck}</div>
          {loading && <div className="error-div">Logging in.</div>}
          {apiError && <div className="error-div">{apiError}</div>}
        </fieldset>
      </form>
    </div>
  );
}
