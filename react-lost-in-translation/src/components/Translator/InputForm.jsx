import { useState } from 'react'
import { useForm } from 'react-hook-form'
import a from "../../images/a.png"
import b from "../../images/b.png"
import c from "../../images/c.png"
import d from "../../images/d.png"
import e from "../../images/e.png"
import f from "../../images/f.png"
import g from "../../images/g.png"
import h from "../../images/h.png"
import i from "../../images/i.png"
import j from "../../images/j.png"
import k from "../../images/k.png"
import l from "../../images/l.png"
import m from "../../images/m.png"
import n from "../../images/n.png"
import o from "../../images/o.png"
import p from "../../images/p.png"
import q from "../../images/q.png"
import r from "../../images/r.png"
import s from "../../images/s.png"
import t from "../../images/t.png"
import u from "../../images/u.png"
import v from "../../images/v.png"
import w from "../../images/w.png"
import x from "../../images/x.png"
import y from "../../images/y.png"
import z from "../../images/z.png"


const msgConfig = {
    required: true,
    maxLength: 40
}

let translationArray = [];
let imagesArray = {
    a: a, b: b, c: c, d: d, e: e, f: f, g: g, h: h, i: i, j: j, k: k,
    l: l, m: m, n: n, o: o, p: p, q: q, r: r, s: s, t: t, u: u, v: v, w: w, x: x, y: y, z: z };

const InputForm = ({ onTranslate }) => {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm();
    

    const [ text, setText ] = useState("");

    const handleOnSubmit = ({ translateMsg }) => {
        onTranslate(translateMsg);

        let newTranslation = translateMsg.split("");
        translationArray = [];

        for(let i=0;i<newTranslation.length;i++){
            translationArray.push(newTranslation[i]);
        }

        console.log(translationArray);
    }

    // this could work with a little more time, need to map the values to the imported variables

    /* function loopImages() {
        for(let i=0;i<translationArray.length;i++){
            console.log("TESTING"+translationArray[i]);
            //imageElement.src = element;
            //<img src={translationArray[i]}></img>
        }
    } */

    const handleChange = (e) => {
        setText(e.target.value); // event - input field (target) - value of input field
        console.log("Hello there");
    }

    const errorMessage = (() => {

        if (!errors.translateMsg) {
    
          return null;
    
        }
    
        if (errors.translateMsg.type === "required") {
    
          return <span>A message is required!</span>;
    
        }
    
        if (errors.translateMsg.type === "maxLength") {
    
          return <span>No more than 40 characters!</span>;
    
        }
    
      })();

  return (
    <>
      <h2>User Input</h2>
      <form className="translate-form" onSubmit={handleSubmit(handleOnSubmit)}>
        <div className="translate-box">
          <fieldset className="translate-fieldset">
            <label className="trns-msg" htmlFor="translateMsg">
              {" "}
            </label>
            <input
              className="login-input"
              autocomplete="off"
              placeholder="Input text"
              type="text"
              {...register("translateMsg", msgConfig)}
              onChange={handleChange}
            ></input>
            <button className="button translate-button" type="submit">
              Translate!
            </button>
            {errorMessage}
          </fieldset>
        </div>
        <h2 id="translationHeader">Translation Output</h2>
        <div id="outputBox">
            {text.split("").map(item => {
                return(<img src={imagesArray[item]} width="75px" height="75px"></img>)}
                )}
        
        </div>
      </form>
    </>
  );
};

export default InputForm;
