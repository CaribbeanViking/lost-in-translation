import { useUser } from "../../context/UserContext";
import { storageRemove } from "../../utils/storage"

export default function LogoutButton() {
  const { setUser } = useUser();
  const handleLogoutClick = async () => {
    if (!window.confirm("Sure you want to log out?")) {
      return null;
    }
    storageRemove("user");
    setUser(null);
  };

  return (
    <button onClick={handleLogoutClick} className="button logout-button">
      Logout
    </button>
  );
}
