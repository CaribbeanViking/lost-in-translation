import OutputHistoryItem from "./OutputHistoryItem";

export default function OutputHistory({ translations }) {
  const translationList = translations.map((translation) => (
    <OutputHistoryItem key={translation} translation={translation} />
  )).reverse();

const lastTenItems = translationList.slice(0,10);

  return (
    <div className="output-history-container">
      <section className="output-history-box">
        <ul className="history-list">{lastTenItems}</ul>
      </section>
    </div>
  );
}
