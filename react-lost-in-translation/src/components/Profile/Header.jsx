import LogoutButton from './LogoutButton.jsx'

export default function Header({username}) {
/*   const logout = () => {
    storageSave(STORAGE_KEY_USER, null);
    setUser(null);
  } */
  return (
    <div>
      <div className="upper-header"><LogoutButton /* logout={logout} *//></div>
      <div className="lower-header"><p className="trans-text">Here are your latest translations, {username}!</p></div>
    </div>
  );
}
