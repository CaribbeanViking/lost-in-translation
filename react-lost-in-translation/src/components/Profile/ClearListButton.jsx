import React from "react";
import { clearTranslations } from "../../api/translations";
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";

export default function ClearListButton() {
  const { user, setUser } = useUser();

  const handleClearList = async () => {
    if (!window.confirm("Sure?")) {
      return null;
    }
    let [clearError, ] = await clearTranslations(user.id);
    const updatedUser = { ...user, translations: [] };

    setUser(updatedUser);
    storageSave(updatedUser);

    if (clearError !== null) {
      return;
    }
  };
  return (
    <button onClick={handleClearList} className="button clListBtn">
      Clear List
    </button>
  );
}
